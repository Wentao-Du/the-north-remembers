# NDK Samples
***

This repository contains [OpenHarmony NDK Samples](https://wentao-du.gitee.io/the-north-remembers) with DevEco Studio C++ integration.

These samples use the [CMake OpenHarmony](https://wentao-du.gitee.io/the-north-remembers/#/toolchains/toolchains) plugin with C++ support.

Known Issues
- For DevEco related issues, refer to [DevEco Studio known issues](https://developer.huawei.com/consumer/cn/forum/block/application) page
- For NDK issues, refer to [ndk issues](https://gitee.com/openharmony/docs/issues)

Additional DevEco Studio samples:
- [applications_app_samples](https://gitee.com/openharmony/applications_app_samples)
- [applications_sample_wifi_iot](https://gitee.com/openharmony/applications_sample_wifi_iot)
- [applications_sample_camera](https://gitee.com/openharmony/applications_sample_camera)

Documentation
- [application-dev-guide](https://docs.openharmony.cn/pages/v3.2/zh-cn/application-dev/application-dev-guide.md/)
- [gitee docs](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/Readme-CN.md)


## Build Steps
- With DevEco Studio:"Open An Existing Project" or File -> New -> Create Project
***

## Support

For any issues you found in these samples, please
- submit patches with pull requests, see [CONTRIBUTING.md](/CONTRIBUTING.md) for more details, or
- [create bugs](https://gitee.com/Wentao-Du/the-north-remembers/issues) here

For OpenHarmony NDK generic questions, please ask on [Issue Report](https://issuereporter.developer.huawei.com/my-created), OpenHarmony teams are periodically monitoring questions here.

***
## License

Copyright 2023 The OpenAtom Foundation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.