### 场景：任务 B 需要在耗时任务 A 后调用，并且使用了任务 A 中操作后的数据
### 流程：

1. 定义数据结构体，并实例化；作为任务 A、B 的数据传输用。
2. napi_create_async_work 传入任务 A、任务 B，创建异步任务，保存在 AsyncWorkInfo->work。
3. napi_queue_async_work 执行上步创建的异步 work。
4. napi_delete_async_work 删除异步 work，释放内存。

### JS 侧
```javascript
Row() {
  Column() {
    Text("Start AsyncWork")
      .fontSize(50)
      .fontWeight(FontWeight.Bold)
      .onClick(() => {
        hilog.info(0x0000, 
                   'testTag', 
                   'Test NAPI executeAsyncWork', 
                   testNapi.executeAsyncWork()
                  );
      })
  }
  .width('100%')
}
```

### C++ 侧
```C++
// 定义数据结构体
struct AsyncWorkInfo {
    napi_async_work work;
    int sum;
};

// 实例化结构体
struct AsyncWorkInfo data = { nullptr, 0 };

// 任务 A 耗时任务，for 循环操作 data
static void AExecute(napi_env env, void *data) {
    struct AsyncWorkInfo *arg = (struct AsyncWorkInfo *)data;
    for(int n = 0; n < 100000; n++) {
        arg->sum += 1;
    }
}

// 任务 B , 拿到任务 A 操作后的数据
static void BComplete(napi_env env, napi_status status, void *data) {
    struct AsyncWorkInfo *arg = (struct AsyncWorkInfo *)data;
    OH_LOG_INFO(LOG_APP, "workB result is %{pubilc}d", arg->sum);
  // 释放内存
    napi_delete_async_work(env, arg->work);
}

static napi_value executeAsyncWork(napi_env env, napi_callback_info info)
{
    napi_value resourceName;
    napi_create_string_utf8(env, "asyncWork", NAPI_AUTO_LENGTH, &resourceName);
    struct AsyncWorkInfo *ptr = &data;
    napi_create_async_work(env, nullptr, resourceName, AExecute, BComplete, ptr, &ptr->work);
    napi_queue_async_work(env, ptr->work);
    return resourceName;
}

```