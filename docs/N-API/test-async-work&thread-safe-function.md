## 前言
> 从用法和官方介绍角度来看，napi_threadsafe_function 和 napi_async_work 似乎是两个完全不相同的两个东西，但从源码分析他们实现的原理，是有很大重合度。以至于使用场景亦有重合的地方。

**读前须知**

- uv_async_send、uv_async_init、uv_queue_work 具体用法和作用。
- napi_async_work 系列 API 具体用法和作用。
- 线程安全函数 API 具体用法和作用。
- Libuv 线程池里的线程都有 napi env 环境和都持有一个 event-loop。
- JS  主线程是 Libuv 线程池中某一个线程，napi_queue_async_work 创建的线程也在 Libuv 线程池里。

## napi_async_work

##### napi_async_work 解决了什么问题？
对于耗时的操作。如果在 Libuv 的主线程里执行的话， 就会阻塞后面的任务执行。所以 Libuv 里维护了一个线程池。简单的说，napi_async_work 负责创建一个新线程放入 Libuv 线程池，去执行耗时操作，待线程执行结束，将回调函数 push 到主线程的 eventloop 内等待被执行。

##### 源码分析 (请下载 node.js 源码，并且结合源码阅读)
使用 napi_create_async_work 创建异步 work；源码中实例化了 uvimpl::Work，把实例引用地址赋值给 result。
```cpp
napi_create_async_work(napi_env env,
                       napi_value async_resource,
                       napi_value async_resource_name,
                       napi_async_execute_callback execute,
                       napi_async_complete_callback complete,
                       void* data,
                       napi_async_work* result) {
  uvimpl::Work* work = uvimpl::Work::New(reinterpret_cast<node_napi_env>(env),
                                         resource,
                                         resource_name,
                                         execute,
                                         complete,
                                         data);

  *result = reinterpret_cast<napi_async_work>(work);

  return napi_clear_last_error(env);
}
```

uvimpl::Work 实际上继承了 ThreadPoolWork，所以说上面的实例是 ThreadPoolWork 实例
```cpp
class Work : public node::AsyncResource, public node::ThreadPoolWork {
 private:
  explicit Work(node_napi_env env,
                v8::Local<v8::Object> async_resource,
                v8::Local<v8::String> async_resource_name,
                napi_async_execute_callback execute,
                napi_async_complete_callback complete = nullptr,
                void* data = nullptr)
      : AsyncResource(
            env->isolate,
            async_resource,
            *v8::String::Utf8Value(env->isolate, async_resource_name)),
        ThreadPoolWork(env->node_env(), "node_api"),
        _env(env),
        _data(data),
        _execute(execute),
        _complete(complete) {}

  ~Work() override = default;
}
```

async work 创建后，需要使用 napi_queue_async_work 创建一个线程执行 work，并把该线程放到线程池里。源码中调用了 w->ScheduleWork()。
```cpp
napi_status NAPI_CDECL napi_queue_async_work(napi_env env,
                                             napi_async_work work) {
  uvimpl::Work* w = reinterpret_cast<uvimpl::Work*>(work);
  w->ScheduleWork();
  return napi_clear_last_error(env);
}
```

ScheduleWork 调用了 libuv api uv_queue_work，创建新线程，第三个函数参数是线程的主行函数
```cpp
void ThreadPoolWork::ScheduleWork() {
	int status = uv_queue_work(
		env_->event_loop(),
		&work_req_,
		[](uv_work_t* req) {
			self->DoThreadPoolWork();
		},
		[](uv_work_t* req, int status) {
			self->AfterThreadPoolWork(status);
		}
	);
}
```

重写了 DoThreadPoolWork 和 AfterThreadPoolWork
```cpp
void DoThreadPoolWork() override { _execute(_env, _data); }

void AfterThreadPoolWork(int status) override {
    _env->CallbackIntoModule<true>([&](napi_env env) {
        _complete(env, ConvertUVErrorCode(status), _data);
    });
}
```
init_threads 用于创建 napi_queue_async_work 需要的线程，并放到线程池中，线程的个数取决于 napi_queue_async_work 被调用了几次；uv_thread_create_ex 创建单个线程，worker 是主函数。
```cpp
uv_queue_work() -> uv__work_submit() -> uv_once(&once, init_once) -> init_threads()
    -> uv_thread_create_ex(threads + i, &config, worker, &sem) -> 执行线程主函数 worker
```
下面看 worker 干了什么，
```cpp
static void worker(void* arg) {
	struct uv__work* w;
	struct uv__queue* q;
	w = uv__queue_data(q, struct uv__work, wq);
	// 执行线程主函数，对应 execute 函数参数
	w->work(w);
	w->work = NULL;
	// 把该线程的新创建的异步回调任务，放到该线程的eventloop里
	uv__queue_insert_tail(&w->loop->wq, &w->wq); 
	// 唤醒持有async的 JS 主线程 eventLoop，并将 async 的回调，放入 JS 主线程的 eventloop
	uv_async_send(&w->loop->wq_async);
}
```
为什么可以唤醒 JS 主线程，因为 &w->loop->wq_async 是在 JS 主线程 initloop 时创建的
```cpp
int uv_loop_init(uv_loop_t* loop) {
	uv_async_init(loop, &loop->wq_async, uv__work_done);
}
```
uv__work_done 被放入了 JS 主线程的 eventloop
```c
void uv__work_done(uv_async_t* handle) {
    // done 对应 complete 函数参数
    w->done(w, err);
}
```
##### 总结

- 在 napi_queue_async_work 后，N-API 会调用 Libuv 库函数 uv_queue_work，新创建一个 Libuv 线程，线程的主函数会执行 napi_async_execute_callback。这个线程的主函数可以使用 napi env 环境。
- JS 主线程初始化 eventloop 时候，调用 uv_async_init，使 JS 主线程持有 async 实例。上述的线程主函数执行结束，通过调用 uv_async_send(&w->loop->wq_async)，唤醒持有 async 的 JS 主线程 eventLoop，并将 napi_async_complete_callback，放入 JS 主线程的 eventloop。
______

## napi_threadsafe_function
##### napi_threadsafe_function 解决了什么问题？
JS 函数只能被JS主线程调用。如果 native 创建的其他线程需要调用 JS 函数，则需要拿到 JS 主线程的 napi_env 来调用 N-API 函数。 这些其他线程必须与 JS 主线程进行通信，线程安全函数API提供了一个简单的方法来实现这一点。
##### 源码分析 (请下载 node.js 源码，并且结合源码阅读)
线程安全函数的使用是，先 napi_create_threadsafe_function 创建函数引用，napi_call_threadsafe_function 通过这个函数引用调用线程安全函数。

- napi_create_threadsafe_function 先实例化 v8impl::ThreadSafeFunction。
- 触发 v8impl::ThreadSafeFunction 的 init() 函数。
- 将 v8impl::ThreadSafeFunction 实例转换成 napi_threadsafe_function 实例，复制给 result。
```cpp
napi_create_threadsafe_function(napi_env env,
                                napi_value func,
                                napi_value async_resource,
                                napi_value async_resource_name,
                                size_t max_queue_size,
                                size_t initial_thread_count,
                                void* thread_finalize_data,
                                napi_finalize thread_finalize_cb,
                                void* context,
                                napi_threadsafe_function_call_js call_js_cb,
                                napi_threadsafe_function* result) {
  v8impl::ThreadSafeFunction* ts_fn = new v8impl::ThreadSafeFunction(v8_func,
                                         v8_resource,
                                         v8_name,
                                         initial_thread_count,
                                         context,
                                         max_queue_size,
                                         reinterpret_cast<node_napi_env>(env),
                                         thread_finalize_data,
                                         thread_finalize_cb,
                                         call_js_cb);
  status = ts_fn->Init();
  if (status == napi_ok) {
  	*result = reinterpret_cast<napi_threadsafe_function>(ts_fn);
  }
  return napi_set_last_error(env, status);
}
```

init 函数主要做了下面几件事情:

- 获取 JS 主线程 loop。
- uv_async_init 初始化 Async Handle（handle 是用来操作线程的），将 AsyncCb 与 async 绑定（AsyncCb 后面会讲述）。
- env->node_env()->CloseHandle 关闭 JS 主线程 Handle（不代表结束线程），将线程安全函数 async 和 JS 主线程 handle 合并。为了能通过线程安全函数 async 操作 JS 主线程。
```cpp
napi_status Init() {
	ThreadSafeFunction* ts_fn = this;
	uv_loop_t* loop = env->node_env()->event_loop();

	if (uv_async_init(loop, &async, AsyncCb) == 0) {
		env->node_env()->CloseHandle(
			reinterpret_cast<uv_handle_t*>(&async),
			[](uv_handle_t* handle) -> void {
				ThreadSafeFunction* ts_fn =
				node::ContainerOf(&ThreadSafeFunction::async,
				reinterpret_cast<uv_async_t*>(handle));
				delete ts_fn;
			});

		// Prevent the thread-safe function from being deleted here, because
		// the callback above will delete it.
		ts_fn = nullptr;
	}
}
```
线程安全函数创建后，需要在其他线程使用 napi_call_threadsafe_function 调用线程安全函数。其将 napi_threadsafe_function 实例转换成 v8impl::ThreadSafeFunction，为了调用 Push 方法。
```cpp
napi_call_threadsafe_function(napi_threadsafe_function func,
                              void* data,
                              napi_threadsafe_function_call_mode is_blocking) {
  return reinterpret_cast<v8impl::ThreadSafeFunction*>(func)->Push(data,
                                                                   is_blocking);
}
```
uv_async_send(&async) 可以唤醒持有async的消息队列，并调用async的回调（上述 uv_loop_init 时传入的  AsyncCb）。
```cpp
napi_status Push(void* data, napi_threadsafe_function_call_mode mode) {
	queue.push(data);
	Send();
	return napi_ok;
}
void Send() {
	CHECK_EQ(0, uv_async_send(&async));
}
```

AsyncCb -> Dispatch ->DispatchOne -> call_js_cb(env, js_callback, context, data); 
```cpp
static void AsyncCb(uv_async_t* async) {
	ThreadSafeFunction* ts_fn =
	node::ContainerOf(&ThreadSafeFunction::async, async);
	ts_fn->Dispatch();
}

void Dispatch() {
	bool has_more = true;
	while (has_more && --iterations_left != 0) {
		has_more = DispatchOne();
	}
	if (has_more) {
		Send();
	}
}
bool DispatchOne() {
	void* data = nullptr;
	bool popped_value = false;
	bool has_more = false;
	env->CallbackIntoModule<false>(
          [&](napi_env env) { 
			  call_js_cb(env, js_callback, context, data); 
		  }
	);
	return has_more;
}
```

肯定会好奇 call_js_cb 是什么函数，它是 napi_create_threadsafe_function 传入的回调函数参数。
##### 总结
napi_create_threadsafe_function 时通过 创建 uv_async 实例 &async 并绑定 AsyncCb，再在其他线程中调用 napi_call_threadsafe_function(tsfn) 后（tsfn 数据结构包含 &async），使用 uv_async_send(&async) 唤醒持有async的 JS 主线程消息队列，并调用async的回调 AsyncCb->call_js_cb()。


async_Work 和 tsfn 使用场景比较二者做的场景逻辑都是一致，在其他线程主函数运行时，回到 JS 主线程，并将回调函数，push 到 JS 主线程的 event-loop队列里等待被执行。

**不同点**

- 创建的线程不一样：Napi_Async_Work 在 queue_async_work 时会自己创建线程，这个线程属于 Libuv 线程池，和 JS 主线程一样具有 event-loop 和 napi_env。Napi_Threadsafe_Function的其他线程时开发自己创建的线程，一般根据业务需要创建的，不在 Libuv 线程池中，也没有 JS 主线程的特性。
- JS 主线程的回调函数被执行得到时机不一致：Napi_Async_Work 必须等到新创建的线程主函数被执行完后，被动被执行，只能被调用一次，Napi_Threadsafe_Function 的回调函数在其他线程中，可以在任意时机主动调用任意次数。

**所以说， Napi_Async_Work 的使用场景是 Napi_Threadsafe_Function 使用场景的子集，但是 Napi_Async_Work 无需开发者创建线程，并且线程具有 event-loop 和 napi_env 特性。**
