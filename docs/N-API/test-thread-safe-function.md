### 场景描述
- JS 侧向 C++ 侧 传入回调函数，回调函数在 C++ 侧 主线程被调用，但是需要使用到 Other 线程处理后的数据。
- 在 ohos 中，javascript 函数只能在 native 插件主线程调用，非主线程不能调用需要napi_env、napi_value、napi_ref 的 NAPI 接口。
- other 线程，处理的数据，需要安全传入主线程，供主线程 js 函数使用。 

**线程安全函数 API 提供一种简化方法，避免线程间通讯，同时可以带着处理后的数据回到主线程调用 js 函数。**

### 流程

1. 主线程 napi_get_cb_info 获取 JS 侧回调函数。
2. 主线程 传入 回调函数 CallJs 创建 napi_create_threadsafe_function。
3. other 线程，创建数据并修改，napi_call_threadsafe_function 调用线程安全函数，将数据传给主线程。
4. 主线程的 CallJs 被调用，在 CallJs 中调用 JS 侧回调函数。

### JS 侧
```javascript
Column() {
	Text("ThreadSafeTest")
		.fontSize(50)
		.fontWeight(FontWeight.Bold)
		.onClick(() => {
			testNapi.ThreadSafeTest((arg) => {
				// 打印 Other 线程处理后的数据
				hilog.info(0x0000, 'testTag', 'the threadId that called jsCb is %{public}d', arg);
			})
		})
}
```

### C++ 侧
```cpp
// 定义线程数据结构体
struct ThreadSafeInfo {
	int sum;
};

// 实例化结构体
struct ThreadSafeInfo threadInfo = { 1 };

static void CallJs(napi_env env, napi_value jsCb, void *context, void *data) {
    napi_value undefined, argv, ret;
    // 获取当前线程 Id
    std::thread::id this_id = std::this_thread::get_id();
    OH_LOG_INFO(LOG_APP, "thread callJs %{public}d\n", this_id);
    // 解析参数 data
    struct ThreadSafeInfo *arg = (struct ThreadSafeInfo *)data;
    napi_create_int32(env, arg->sum, &argv);
    // 调用 js 回调函数
    napi_call_function(env, undefined, jsCb, 1, &argv, &ret);
}

static napi_value ThreadSafeTest(napi_env env, napi_callback_info info)
{
    std::thread::id this_id = std::this_thread::get_id();
    OH_LOG_INFO(LOG_APP, "thread ThreadSafeTest %{public}d\n", this_id);
    size_t argc = 1;
    napi_value workName;
    napi_create_string_utf8(env, "threadSafeTest", NAPI_AUTO_LENGTH, &workName);
    // 获取 js 回调函数
    napi_get_cb_info(env, info, &argc, &jsCb, nullptr, nullptr);
    // 创建线程安全函数
    napi_create_threadsafe_function(env, jsCb, nullptr, workName, 0, 1, nullptr, nullptr,
        nullptr, CallJs, &tsfn);
    std::thread otherTask([]() {
        std::thread::id this_id = std::this_thread::get_id();
        OH_LOG_INFO(LOG_APP, "thread otherTask %{public}d\n", this_id);
        // 定义数据引用
        struct ThreadSafeInfo *data = &threadInfo;
        for(int n = 0; n < 1000; n++) {
            data->sum += 1;
        }
        napi_acquire_threadsafe_function(tsfn);
        // 调用主线程函数，传入 Data
        napi_call_threadsafe_function(tsfn, data, napi_tsfn_blocking);
    });
    otherTask.detach();
    return nullptr;
}
```
**注**：跨线程数据 data 引用指向的实际地址，不能随着 other 线程结束而被销毁