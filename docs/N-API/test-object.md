# C++ 侧对JS Object的基本操作
N-API提供一些API来创建JavaScript所有类型值。这些API用于执行以下操作：

- 创建一个新的JavaScript对象
- 将C类型数据转换成N-API类型数据
- 将N-API类型数据转换成C类型数据

## 场景：创建一个 Object，并对其属性赋值
#### N-API 写法（注释代码是相应的 JS 写法）
```cpp
// let obj = {}
napi_value obj;
napi_create_object(env, &obj);

// let world = "world"
napi_value world;
napi_create_string_utf8(env, "world", NAPI_AUTO_LENGTH, &world);

// let arr = []
napi_value arr, arrElement0, arrElement1, arrElement2;
napi_create_array(env, &arr);

// arr[0] = 94
napi_create_int32(env, 94, &arrElement0);
napi_set_element(env, arr, 0, arrElement0);

// arr[1] = "123d"
napi_create_string_utf8(env, "123d", NAPI_AUTO_LENGTH, &arrElement1);
napi_set_element(env, arr, 1, arrElement1);

// arr[2] = 12.32
napi_create_double(env, 12.32, &arrElement2);
napi_set_element(env, arr, 2, arrElement2);

// let obj1 = {}
napi_value obj1, objValue;
napi_create_object(env, &obj1);

// let objValue = "obj in obj"
napi_create_string_utf8(env, "obj in obj", NAPI_AUTO_LENGTH, &objValue);

// obj1["test"] = objValue
napi_set_named_property(env, obj1, "test", objValue);

// obj["hello"] = world
// obj["arr"] = arr
// obj["obj1"] = obj1
napi_set_named_property(env, obj, "hello", world);
napi_set_named_property(env, obj, "arr", arr);
napi_set_named_property(env, obj, "obj1", obj1);
```
## C++ 侧对 Object 类型数据属性修改
场景：js 端向 C++ 端传入 Object 类型数据，C++端对其属性就行修改后，返回给 js 端。
注：建议先学习 [JS TypedArray](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/TypedArray)
#### 接收参数
##### js 调用 C++ 导出方法 modifyObject，并传参
```typescript
Row() {
  Text("ModifyObject")
    .fontSize(50)
    .fontWeight(FontWeight.Bold)
    .onClick(() => {
      const typedArray = new Uint8Array(6);
      typedArray[0] = 127;
      typedArray[2] = 66;
      typedArray[4] = 32;
      const obj: {
        obj1: Object,
        hello: String,
        arr: Array<any>,
        typedArray: Uint8Array
      } = {
        hello: 'world',
        arr: [94, '123d', 12.32, { test: 'obj in arr'}],
        obj1: {
          test: 'obj in obj'
        },
        typedArray
      }
      hilog.info(0x0000, 'napiTestTag', 'Test NAPI modifyObject result is %{public}s', JSON.stringify(testNapi.modifyObject(obj)));
    })
}
```
##### C++ 解析参数
```cpp
static napi_value modifyObject(napi_env env, napi_callback_info info) {
    // 数组的长度并接收参数的实际计数
    size_t argc = 1;
    napi_value args[1] = { nullptr };
    napi_get_cb_info(env, info, &argc, args, nullptr, nullptr);
    // js端传过来的参数
    napi_value obj = args[0];
}
```
#### 修改 Object 类型数据（注释代码是相应的 JS 写法）
##### N-API 写法
```cpp
// let obj =  {
//   hello: 'world',
//   arr: [94, '123d', 12.32, { test: 'obj in arr'}],
//   obj1: {
//     test: 'obj in obj'
//   }
// }
napi_value obj = args[0];

// let world0 = "world0";
// obj['hello'] = world0
napi_value world0;
napi_create_string_utf8(env, "world0", NAPI_AUTO_LENGTH, &world0);
napi_set_named_property(env, obj, "hello", world0);

// let arr = obj['arr'];
// delete arr[2];
napi_value arr;
napi_get_named_property(env, obj, "arr", &arr);
napi_delete_element(env, arr, 2, nullptr);

// let arrLen;
// arrLen = arr.length;
uint32_t arrLen;
napi_get_array_length(env, arr, &arrLen);

// for(let i = 0; i < arrLen; i++) {
//   let tmp = i;
//   arr[i] = tmp;
// }
uint32_t i = 0;
for(i = 0; i < arrLen; i++) {
	napi_value tmp;
	napi_create_uint32(env, i, &tmp);
	napi_set_element(env, arr, i, tmp);
}

// let typedArr = obj['typedArray']
napi_value typedArr;
napi_get_named_property(env, obj, "typedArray", &typedArr);
bool is_typedarray;
NODE_API_CALL(env, napi_is_typedarray(env, typedArr, &is_typedarray));

// let input_buffer = typedArr.buffer
// let byte_offset = typedArr.byteOffset
// let length = typedArr.length
napi_typedarray_type type;
napi_value input_buffer;
size_t byte_offset;
size_t length;
NODE_API_CALL(env, napi_get_typedarray_info(env, typedArr, &type, &length, NULL, &input_buffer, &byte_offset));

// 获取 input_buffer 的基础数据缓冲区 data，和基础数据缓冲区的长度 byte_length。
void* data;
size_t byte_length;
NODE_API_CALL(env, napi_get_arraybuffer_info(env, input_buffer, &data, &byte_length));

// 创建新的ArrayBuffer，&output_ptr 指向 ArrayBuffer 的底层数据缓冲区的指针
napi_value output_buffer;
void* output_ptr = NULL;
NODE_API_CALL(env, napi_create_arraybuffer(env, byte_length, &output_ptr, &output_buffer));

// 使用 output_buffer 创建 typedarray
napi_value output_array;
NODE_API_CALL(env, napi_create_typedarray(env, type, length, output_buffer, byte_offset, &output_array));

// data 是由连续的内存位置组成，(uint8_t*)(data) 表示其第一个元素的内存地址。
// data 是旧的 arraybuffer 数据指针
uint8_t* input_bytes = (uint8_t*)(data) + byte_offset;

// 把 output_ptr 指针赋值给 output_bytes
// output_ptr 是新的 arraybuffer 数据指针
uint8_t* output_bytes = (uint8_t*)(output_ptr);
for (i = 0; i < length; i++) {
	// 将旧 arraybuffer 数据每一个元素乘 2，赋值给新 arraybuffer 数据
	output_bytes[i] = (uint8_t)(input_bytes[i] * 2);
}

// 将新 typedArray 赋值给 obj['typedArray']
napi_set_named_property(env, obj, "typedArray", output_array);

// let obj1 = obj['obj1'];
napi_value obj1;
napi_get_named_property(env, obj, "obj1", &obj1);

// let str = obj1['test'];
// let buf = str;
// buf += "，this is be modified";
napi_value str;
napi_get_named_property(env, obj1, "test", &str);
char buf;
size_t bufSize = 20;
size_t res;
napi_get_value_string_utf8(env, str, &buf, bufSize, &res);
strcat(&buf, "，this is be modified");

// obj1['test'] = buf;
napi_value newNapiStr;
napi_create_string_utf8(env, &buf, NAPI_AUTO_LENGTH, &newNapiStr);
napi_set_named_property(env, obj1, "test", newNapiStr);

napi_set_named_property(env, obj, "obj1", obj1);
```
##### 修改后输出：
```typescript
Test NAPI modifyObject result is {"hello":"world0","arr":[0,1,2,3],"obj1":{"test":"obj in obj，this is be modified"}}
```
## 相关API
#### 创建 Object

- [napi_create_array](https://nodejs.org/api/n-api.html#napi_create_array)
- [napi_create_array_with_length](https://nodejs.org/api/n-api.html#napi_create_array_with_length)
- [napi_create_arraybuffer](https://nodejs.org/api/n-api.html#napi_create_arraybuffer)
- [napi_create_buffer](https://nodejs.org/api/n-api.html#napi_create_buffer)	
- [napi_create_buffer_copy](https://nodejs.org/api/n-api.html#napi_create_buffer_copy)
- [napi_create_date](https://nodejs.org/api/n-api.html#napi_create_date)
- [napi_create_external](https://nodejs.org/api/n-api.html#napi_create_external)
- [napi_create_external_arraybuffer](https://nodejs.org/api/n-api.html#napi_create_external_arraybuffer)
- [napi_create_external_buffer](https://nodejs.org/api/n-api.html#napi_create_external_buffer)
- [napi_create_object](https://nodejs.org/api/n-api.html#napi_create_object)
- [napi_create_symbol](https://nodejs.org/api/n-api.html#napi_create_symbol)
- [node_api_symbol_for](https://nodejs.org/api/n-api.html#node_api_symbol_for)
- [napi_create_typedarray](https://nodejs.org/api/n-api.html#napi_create_typedarray)
- [napi_create_dataview](https://nodejs.org/api/n-api.html#napi_create_dataview)

#### C 类型数据转换成 N-API 类型数据

- [napi_create_int32](https://nodejs.org/api/n-api.html#napi_create_int32)
- [napi_create_uint32](https://nodejs.org/api/n-api.html#napi_create_uint32)
- [napi_create_int64](https://nodejs.org/api/n-api.html#napi_create_int64)
- [napi_create_double](https://nodejs.org/api/n-api.html#napi_create_double)
- [napi_create_bigint_int64](https://nodejs.org/api/n-api.html#napi_create_bigint_int64)
- [napi_create_bigint_uint64](https://nodejs.org/api/n-api.html#napi_create_bigint_uint64)
- [napi_create_bigint_words](https://nodejs.org/api/n-api.html#napi_create_bigint_words)
- [napi_create_string_latin1](https://nodejs.org/api/n-api.html#napi_create_string_latin1)
- [napi_create_string_utf8](https://nodejs.org/api/n-api.html#napi_create_string_utf8)
#### N-API 类型数据 转换成 C 类型数据

- [napi_get_array_length](https://nodejs.org/api/n-api.html#napi_get_array_length)
- [napi_get_arraybuffer_info](https://nodejs.org/api/n-api.html#napi_get_arraybuffer_info)
- [napi_get_buffer_info](https://nodejs.org/api/n-api.html#napi_get_buffer_info)
- [napi_get_prototype](https://nodejs.org/api/n-api.html#napi_get_prototype)
- [napi_get_typedarray_info](https://nodejs.org/api/n-api.html#napi_get_typedarray_info)
- [napi_get_dataview_info](https://nodejs.org/api/n-api.html#napi_get_dataview_info)
- [napi_get_date_value](https://nodejs.org/api/n-api.html#napi_get_date_value)
- [napi_get_value_bool](https://nodejs.org/api/n-api.html#napi_get_value_bool)
- [napi_get_value_double](https://nodejs.org/api/n-api.html#napi_get_value_double)
- [napi_get_value_bigint_int64](https://nodejs.org/api/n-api.html#napi_get_value_bigint_int64)
- [napi_get_value_bigint_uint64](https://nodejs.org/api/n-api.html#napi_get_value_bigint_uint64)
- [napi_get_value_bigint_words](https://nodejs.org/api/n-api.html#napi_get_value_bigint_words)
- [napi_get_value_external](https://nodejs.org/api/n-api.html#napi_get_value_external)
- [napi_get_value_int32](https://nodejs.org/api/n-api.html#napi_get_value_int32)
- [napi_get_value_int64](https://nodejs.org/api/n-api.html#napi_get_value_int64)
- [napi_get_value_string_latin1](https://nodejs.org/api/n-api.html#napi_get_value_string_latin1)
- [napi_get_value_string_utf8](https://nodejs.org/api/n-api.html#napi_get_value_string_utf8)
- [napi_get_value_string_utf16](https://nodejs.org/api/n-api.html#napi_get_value_string_utf16)
- [napi_get_value_uint32](https://nodejs.org/api/n-api.html#napi_get_value_uint32)

