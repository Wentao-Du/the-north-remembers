#### LibUV
- 是一个高性能事件驱动的异步I/O库，由C语言编写，具有很高的可移植性。
- 封装不同平台底层对于异步IO模型的实现。windows、linux都可以使用。
- 专为 Node.js 设计，为了解决 Web 服务器高并发性能问题。
- 后因其事件驱动的异步IO高效性，逐渐被其他语言和项目采纳为底层库，例如 Ark 引擎。
#### 场景
应用在 native 层有个下载任务，在非 JS 线程里，进程执行期间需要将下载进度传到前端（ JS 侧），并使用进度数据绘制进度条。
#### 方案

1. 前端进度条用 Progress 组件绘制。
2. JS 侧调用 native 侧方法，并传入用于接收下载进度的回调函数。
3. JS 主线程使用 std::thread 启用下载线程。
4. 使用 while 循环每 100 毫秒执行一次 ` uv_queue_work(uv_loop_t* loop, uv_work_t* req, uv_work_cb work_cb, uv_after_work_cb after_work_cb);`  向 eventLoop 事件堆栈 push 异步任务 after_work_cb。
5. 异步任务(after_work_cb) 中 调用napi_call_function 来执行 JS 侧回调，向其传输下载进度数据
####  JS 侧
```typescript
struct Index {
  @State progress:number = 0;
  build() {
    Column() {
      Row() {
        Button("Start Download")
          .fontSize(40)
          .fontWeight(FontWeight.Bold)
          .onClick(() => {
            testNapi.startDownload((data)=>{
              // data 是 native 层传过来的下载进度
              this.progress = data;
              hilog.info(0x0000, 'testTag', 'startDownload NAPI async result is %{public}d', data);
            });
          });
      }.margin({
        top: 10,
        bottom: 10,
        left: 5,
        right:5
      })

      Row() {
        Text(`当前下载进度：${this.progress}%`)
          .fontSize(20)
      }.margin({
        top: 10,
        bottom: 5,
        left: 5,
        right:5
      })

      Row() {
        Progress({ value: this.progress, total: 100, type: ProgressType.Ring })
          .width(100)
          .height(100)
          .animation({ duration: 1000, curve: Curve.Ease })
          .style({ strokeWidth: 15})
      }
    }
  }
}
```
#### C++ 侧
```cpp
struct CallbackContext {
// JS N-API 上下文环境 
napi_env env = nullptr;
// JS 回调函数引用，保证 JS 主线程结束时，回调函数不被销毁
napi_ref callbackRef = nullptr;
// 下载进度数据
int retData = 0;
};

// 模拟下载任务（download 线程任务）
void downloadTask(CallbackContext* context) {
    uv_loop_s* loop = nullptr;
    // 获取 context->env 上下文环境的 loop (线程池）
    napi_get_uv_event_loop(context->env, &loop);

    // uv_work_t 是关联 loop 和 线程池回调函数的结构体
    uv_work_t* work = new uv_work_t;
    work->data = (CallbackContext*)context;

    while (context && context->retData < 100) {
        uv_queue_work(
            loop,
            work, 
            [](uv_work_t* work) {}, 
            [](uv_work_t* work, int status){
                CallbackContext* context = (CallbackContext*)work->data;
                context->retData += 1;
                napi_handle_scope scope = nullptr;
                // 管理 napi_value 的生命周期，防止内存泄露
                napi_open_handle_scope(context->env, &scope);
                napi_value callback = nullptr;
                napi_get_reference_value(context->env, context->callbackRef, &callback);
                napi_value retArg;
                napi_create_int32(context->env, context->retData, &retArg);
                napi_value ret;
                // napi_call_function 调用 JS 侧回调，向其传输下载进度
                napi_call_function(context->env, nullptr, callback, 1, &retArg, &ret);

                napi_close_handle_scope(context->env, scope);
                if (context->retData > 99) {
                    delete context;
                    delete work;
                }
            }
            );
        // 控制 while 循环每 100 毫秒执行一次。
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
};

// JS 主线程
static napi_value startDownload(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1] = { 0 };
    napi_get_cb_info(env, info, &argc, args , nullptr, nullptr);
    auto asyncContext = new CallbackContext();
    asyncContext->env = env;
    napi_create_reference(env, args[0], 1, &asyncContext->callbackRef);
    // 启动下载线程
    std::thread downloadThread(downloadTask, asyncContext);
    downloadThread.detach();
    return nullptr;
};
```
#### Demo演示
![test-libuv](../figures/native-api/test-libuv.gif)