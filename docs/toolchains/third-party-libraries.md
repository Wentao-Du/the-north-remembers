# 三方库迁移至 OpenHarmony 应用
***
三方库是指其他公司或者组织提供的服务或模块，例如常见的开源 OpenCV、libcurl 库，将三方库迁移至OpenHarmony 上，有助于系统的生态建设。

本文首先以三方库 [ curl ](https://github.com/curl/curl) 为例，分别给出 Windows 和 Linux 开发环境下，curl 移植到 OpenHarmony 上的流程示意。主要涉及三部分内容，分别为编译环境的准备、编译工具的使用以及生成文件的调用。然后以 [luajit](https://github.com/LuaJIT/LuaJIT.git) 库为例，介绍非camke编译时，如何配置环境变量，并使用make进行编译构建。

![cmake](../figures/toolchains/flow.png ':size=60%')

***
## <b>1.Windows</b>

#### 编译环境准备
- curl 库的源码下载至本地，并新建一个build文件夹

```
git clone https://github.com/curl/curl.git
```
- native中cmake的相关工具添加到系统环境变量，cmake编译工具位于OpenHarmony SDK的native目录下

   - HUAWEI DevEco Studio[下载安装](https://developer.harmonyos.com/cn/develop/deveco-studio)
   - DevEco 中可以安装OpenHarmony SDK，也可以手动[下载安装](https://gitee.com/openharmony/docs/tree/master/zh-cn/release-notes)

![cmake](../figures/toolchains/cmake.png ':size=40%')

#### 动态库的编译


- libcurl.so 的编译

```
mkdir build
cd build

# 编译64或者32，二选一，选择 Ninja 生成器
# arm64-v8a 编译64位
cmake -DCMAKE_TOOLCHAIN_FILE=C:\Users\duwentao\AppData\Local\OpenHarmony\Sdk\9\native\build\cmake\ohos.toolchain.cmake -DOHOS_ARCH=arm64-v8a -DOHOS_STL=c++_shared .. -G Ninja

# lib下的cmake

# armeabi-v7a 编译32位
cmake -DCMAKE_TOOLCHAIN_FILE=C:\Users\duwentao\AppData\Local\OpenHarmony\Sdk\9\native\build\cmake\ohos.toolchain.cmake -DOHOS_ARCH=armeabi-v7a -DOHOS_STL=c++_shared .. -G Ninja

# 编译构建
cmake --build .
```

- cmake参数介绍
   - OHOS_STL，目标文件类型，可以是 c++_shared 和 c++_static，默认是c++_shared
   - OHOS_ARCH，设置应用程序二进制接口ABI，可以是 armeabi-v7a 和 arm64-v8a，默认值 arm64-v8a
   - CMAKE_TOOLCHAIN_FILE，工具链文件所在的位置，即 ohos.toolchain.cmake 的路径
   - -G Ninja，使用生成器Ninja

#### libcurl.so 的调用

   - so 文件放入对应目录下

    ![cmake](../figures/toolchains/curl1.png ':size=20%')

   - 在 CMakeLists.txt 中链入对应so，需要根据编译出来的abiFilters类型，将so放入对应文件夹

    ![cmake](../figures/toolchains/curl2.png ':size=40%')

   - 在 CPP 代码中调用头文件中的接口。也可以通过NAPI的方式封装C++接口，然后在JS侧进行调用，这种方式会在后续介绍

    ![cmake](../figures/toolchains/curl3.png ':size=30%')
    
***

## <b>2.Linux</b>
编译环境目前主要支持Ubuntu18.04和Ubuntu20.04（Ubuntu22.04暂不支持）

#### 编译环境准备
本节基于wsl和Ubuntu20.04，在Linux环境下编译 curl 库，并在应用中调用 curl 中的相关接口

- curl 库的源码下载至本地，并新建一个build文件夹

```
git clone https://github.com/curl/curl.git
```

- 下载 OpenHarmony SDK 命令行工具
   - SDK下载 [地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_cli) 及ohsdkmgr使用 [指导](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/ide-command-line-ohsdkmgr-0000001545647965-V3?catalogVersion=V3)
   - 解压命令行工具，工具名称请根据实际情况进行修改
   ```
   unzip command-line-tools.zip
   ```
   - 打开SDK工具配置文件config.properties，并设置OpenHarmony SDK存储路径，请注意，指定路径时请使用绝对路径
   ```
   vim config.properties
   ```
   ![cmake](../figures/toolchains/sdkconfig.png ':size=30%')
   - 进入到command-line-tools/sdkmanager/bin下，安装OpenHarmony SDK，SDK中包含ArkTS、JS和Native SDK，以及toolchains工具链。例如，同时安装native和toolchains
   ```
   ./sdkmgr install native:9 toolchains --accept-license
   ```
   - 配置SDK环境变量
   ```
   vim ~/.bashrc
   # 配置路径
   export PATH='/home/dwt/ndk/9/native/build-tools/cmake/bin':$PATH
   # 执行source命令使配置文件立即生效
   source ~/.bashrc  
   ```

#### 动态库的编译
- curl代码下载完成后，在curl目录下，按照以下步骤编译

```
mkdir build
cd build
cmake -DCMAKE_TOOLCHAIN_FILE=//home/dwt/ndk/9/native/build/cmake/ohos.toolchain.cmake -DOHOS_ARCH=arm64-v8a -DOHOS_STL=c++_shared .. -G Ninja -DHAVE_WRITABLE_ARGV_EXITCODE=0 -DHAVE_WRITABLE_ARGV_EXITCODE__TRYRUN_OUTPUT=0
```
   - DHAVE_WRITABLE_ARGV_EXITCODE和DHAVE_WRITABLE_ARGV_EXITCODE__TRYRUN_OUTPUT为cmake中 [try_run()函数](https://cmake.org/cmake/help/latest/command/try_run.html) 所需缓存变量

- 在 /curl/build/lib 目录下将会生成目标文件：libcurl.so

#### libcurl.so 的调用
参照上文windows示例的调用方式，开发者可在业务native代码中调用curl相关接口

***

## <b>3.非cmake编译</b>
[luajit](https://github.com/LuaJIT/LuaJIT.git) 是一个以makefile进行构建的三方库。这种情况下有两种处理策略，一种是使用 cmake 构建脚本重写开源库的构建过程，对于cmake的命令语法要求较高。另一种是直接使用开源库原生的构建工具，例如 configure、makefile，然后配置ohos_sdk的环境变量，进行编译。

![cmake](../figures/toolchains/luajit.png ':size=30%')

#### 方法一：使用 cmake 构建脚本重写开源库的构建过程
- 请参考 oh 三方库 cmake [编写指导](https://docs.openharmony.cn/pages/v3.2/en/device-dev/porting/porting-thirdparty-cmake.md/#cross-compilation-settings)

#### 方法二：使用开源库原生的构建工具，例如 configure、makefile
首先，编译工具都有自己对应的环境变量。构建脚本并不是直接调用 gcc 或者 clang 编译工具链的，而是优先调用 CC 环境变量以达到调用 c 编译器的目的，对应的 CXX 环境变量对应 c++ 编译器。通过设置环境变量，让构建工具使用 ohos SDK 的编译工具链。

- 设置 ohos 64bit 库编译工具链环境变量
```
#aarch64-linux-ohos
export OHOS_SDK=/home/dwt/ndk/9
export AS=${OHOS_SDK}/native/llvm/bin/llvm-as
export CC="${OHOS_SDK}/native/llvm/bin/clang --target=aarch64-linux-ohos"
export CXX="${OHOS_SDK}/native/llvm/bin/clang++ --target=aarch64-linux-ohos"
export LD="${OHOS_SDK}/native/llvm/bin/lld --target=aarch64-linux-ohos"
export STRIP=${OHOS_SDK}/native/llvm/bin/llvm-strip
export RANLIB=${OHOS_SDK}/native/llvm/bin/llvm-ranlib
export OBJDUMP=${OHOS_SDK}/native/llvm/bin/llvm-objdump
export OBJCOPY=${OHOS_SDK}/native/llvm/bin/llvm-objcopy
export NM=${OHOS_SDK}/native/llvm/bin/llvm-nm
export AR=${OHOS_SDK}/native/llvm/bin/llvm-ar
export CFLAGS="-fPIC -D__MUSL__=1"
export CXXFLAGS="-fPIC -D__MUSL__=1"
```
- 设置 ohos 32bit 库编译工具链环境变量
```
linux-arm
export OHOS_SDK=/home/dwt/ndk/9
export AS=${OHOS_SDK}/native/llvm/bin/llvm-as
export CC="${OHOS_SDK}/native/llvm/bin/clang --target=arm-linux-ohos"
export CXX="${OHOS_SDK}/native/llvm/bin/clang++ --target=arm-linux-ohos"
export LD="${OHOS_SDK}/native/llvm/bin/lld --target=arm-linux-ohos"
export STRIP=${OHOS_SDK}/native/llvm/bin/llvm-strip
export RANLIB=${OHOS_SDK}/native/llvm/bin/llvm-ranlib
export OBJDUMP=${OHOS_SDK}/native/llvm/bin/llvm-objdump
export OBJCOPY=${OHOS_SDK}/native/llvm/bin/llvm-objcopy
export NM=${OHOS_SDK}/native/llvm/bin/llvm-nm
export AR=${OHOS_SDK}/native/llvm/bin/llvm-ar
export CFLAGS="-fPIC -march=armv7a -D__MUSL__=1"
export CXXFLAGS="-fPIC -march=armv7a -D__MUSL__=1"
```
- 编译
```
make
```
![cmake](../figures/toolchains/luajit_so.png ':size=30%')