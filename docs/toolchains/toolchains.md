# 鸿蒙NDK使用入门
***
在DevEco Studio中默认使用的编译构建系统和编译工具是 [CMake](https://cmake.org/) 与 [Ninjia](https://ninja-build.org/)。很多现存项目使用的编译工具是 [Make](https://www.gnu.org/software/make/manual/make.html)， 我们尝试使用 make 工具对一些三方库进行构建，会偶现一些兼容性问题，因此我们推荐您使用 CMake 与 Ninjia。不过，如果您要创建新的原生库，则应使用 CMake 与 Ninjia。


## 下载 NDK 和工具
如需为您的应用编译和调试原生代码，您需要以下组件：
- OpenHarmony 原生开发套件（NDK）：这套工具使您能在 OpenHarmony 应用中使用 C 和 C++ 代码；
- CMake：一款外部构建工具，可与 Gradle 搭配使用来构建原生库；
- GDB：DevEco Studio 用于调试原生代码的调试程序；

如需了解如何安装这些组件，请参阅安装及配置 [OpenHarmony SDK](https://gitee.com/openharmony/docs/tree/master/zh-cn/release-notes)，当然，也可以通过[DevEco Studio](https://developer.harmonyos.com/cn/develop/deveco-studio)的途径下载 SDK。
例如，获取标准系统的 Public SDK 包（Windows/Linux）：

![cmake](../figures/toolchains/sdk_download.png ':size=40%')

下载的SDK解压后，可以看到 NDK 开发中涉及到的主要工具，包括 cmake、llvm 和 sysroot三部分：

![cmake](../figures/toolchains/native_arch.png ':size=40%')

## 创建或导入原生项目
DevEco Studio 设置完成后，支持直接创建新的 C/C++ 项目。但如果您想向现有 DevEco Studio 项目添加或导入原生代码，则需要按以下基本流程操作：
- 创建新的原生源代码文件，并将其添加到 DevEco Studio 项目中。
   - 如果您已经拥有原生代码或想要导入预构建原生库，则可跳过此步骤。
- 创建 CMake 构建脚本，利用 CMake 工具将原生源文件构建成库。

- 通过点击 Run 图标 从主菜单运行应用 构建并运行应用。Gradle 会以依赖项的形式添加 CMake  进程，用于编译和构建原生库并将其随 HAP 一起打包。
应用在实体设备或模拟器上运行后，您可以使用 DevEco Studio 调试应用。如需详细了解 NDK 及其组件，请关注下文。
