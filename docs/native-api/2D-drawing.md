# 使用drawing在xcomponent中添加字体



## 场景介绍

将Native Drawing实现的2D图形绘制和文本绘制在xcomponent中展示。



## 流程图

![drawing](../figures/native-api/drawing/drawing.png)



## 方案说明

### 一、使用[Drawing](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/napi/drawing-guidelines.md#drawing%E5%BC%80%E5%8F%91%E6%8C%87%E5%AF%BC)获取到bitmapAddr

1. **创建Bitmap实例**。使用 **drawing_bitmap.h** 的 **OH_Drawing_BitmapCreate** 接口创建一个Bitmap实例 **cBitmap**，并使用 **OH_Drawing_BitmapBuild** 指定其长宽大小和像素格式。

   

2. **创建画布实例**。使用 **drawing_canvas.h** 的 **OH_Drawing_CanvasCreate** 接口创建一个画布实例 **cCanvas**，并使用 **OH_Drawing_CanvasBind** 接口将 **cBitmap** 实例绑定到 **cCanvas** 上，后续在画布上绘制的内容会输出到绑定的 **cBitmap** 实例中。

   

3. **构造Path形状**。使用 **drawing_path.h** 提供的接口完成一个五角星形状的构造 **cPath**。

   

4. **设置画笔和画刷样式**。使用 **drawing_pen.h** 的 **OH_Drawing_PenCreate** 接口创建一个画笔实例 **cPen**, 并设置抗锯齿、颜色、线宽等属性，画笔用于形状边框线的绘制。使用**drawing_brush.h** 的 **OH_Drawing_BrushCreate** 接口创建一个画刷实例 **cBrush**, 并设置填充颜色， 画刷用于形状内部的填充。使用 **drawing_canvas.h** 的 **OH_Drawing_CanvasAttachPen** 和 **OH_Drawing_CanvasAttachBrush** 接口将画笔画刷的实例设置到画布实例中。

   

5. **绘制Path形状**。使用 **drawing_canvas.h** 的 **OH_Drawing_CanvasDrawPath** 接口将五角星绘制到画布上，绘制完毕后不再使用的实例需要调用对应的接口进行销毁。

   

6. **获取像素数据**。使用 **drawing_bitmap.h** 的 **OH_Drawing_BitmapGetPixels** 接口获取到画布绑定bitmap实例的像素地址，该地址指向的内存包含画布刚刚绘制的像素数据。

## 二、拿到bitmapAddr后使用[OpenGl ES](https://learnopengl-cn.readthedocs.io/zh/latest/)在xcomponent渲染

```c++
//egl_core.cpp

glActiveTexture(GL_TEXTUREO)//在绑定纹理之前先激活纹理单元
glBindTexture(GL_TEXTURE_2D,textureId)//将一个命名的纹理绑定到一个纹理目标上
glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,width_,height_,0,GL_RGBA,GL_UNSIGNED_BYTE,bitmapAddr)//使用bitmapAddr生成纹理
glBindTexture(GL_TEXTURE_2D,GL_NONE)//生成纹理和相应的多级渐远纹理后，释放图像的内存并解绑纹理
glViewport(0,0,width_,height_)//标准化设备坐标映射到屏幕坐标
glClearColor(0.0,0.0,0.0,1.0)//设置颜色
glClear(GL_COLOR_BUFFER_BIT)//清空屏幕的颜色缓冲
glUseProgram(mProgramHandle)//激活这个程序对象
    
//Load the vertext position
glEnableVertexAttribArray(0)
glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,3*sizeof(GLfloat),textureCoords)//指定缓冲内容顶点数组的属性的布局


//Load the texture coordinate
glEnableVertexAttribArray(1)
glVertexAttribPointer(1,2,GL_FLOAT,GL_FALSE,2*sizeof(GLfloat),textureCoords)//指定缓冲内容顶点数组的属性的布局
    
glActiveTexture(GL_TEXTUREO)//激活纹理单元
glBindTexture(GL_TEXTURE_2D,textureId)//绑定纹理
glUniform1i(loc,0)//给纹理采样器分配一个位置值
glDrawElements(GL_TRIANGLES,6,GL_UNSIGNED_SHORT,indices)//使用当前绑定的索引缓冲对象中的索引进行绘制
    
glFlush()//将GL命令队列中的命令发送给显卡并清空命令队列，发送完立即返回
glFinish()//将GL命令队列中的命令发送给显卡并情况命令队列，显卡完成这些命令(也就是画完了)后返回
eglSwapBuffers(mEGLDisplay,mEGLSurface)//获取opengl画出的画面，保存图片供使用
```



### 三、根据[xcomponent开发指导](https://gitee.com/Wentao-Du/the-north-remembers/blob/master/docs/native-api/xcomponent.md)，将native侧中暴露的方法映射到eTS侧

1. 在plugin_render.cpp中注册xcomponent事件回调，创建EGL/OpenGLES环境，exports添加字体的方法
2. 在cmake中导入模块编译成so
3. ets中引入so文件，在surface类型的xcomponent中通过onload拿到context，即可使用挂载在上面方法