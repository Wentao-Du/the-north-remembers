# hilog

## 基本使用

hilog的native api和js api基本一致,主要包含两个步骤:

1. 首先定义 `LOG_DOMAIN` 和 `LOG_TAG`,基本原则: 同一个应用的`LOG_TAG`保持一致,应用里面的不同模块可以通过定义不同的`LOG_DOMAIN`来区分.建议新建一个`log_config.h`头文件

```cpp
// log_config.h
#ifndef cpp_log_config_h
#define cpp_log_config_h

#ifndef LOG_DOMAIN
#define LOG_DOMAIN 0xff1f // 取值范围:[0x0~0xFFFF],如果应用内模块有日志划分的诉求,建议通过该宏定义实现
#endif

#ifndef LOG_TAG
#define LOG_TAG "testApp" // 应用标签 
#endif

#include "hilog/log.h"
#endif
```

2. 修改应用工程的`CMakeLists.txt`链接`hilog`模块的so:`libhilog_ndk.z.so`

```cmake
target_link_libraries(entry PUBLIC libace_napi.z.so libhilog_ndk.z.so)
```

3. 日志输出,调用对应的宏定义函数输出对应级别的日志, 例如输出

```cpp
OH_LOG_INFO(LogType::LOG_APP, "%{public}lf, %lf", value0, value1);
```

```
05-20 16:41:21.761 10767-10767/com.example.myapplication I A0ff1f/testApp: 2.000000, <private>
```

4. 日志查看,通过`DevEco Studio`的日志工具查看或者通过`hdc`工具进入shell查看对应日志

`hilog -T testApp` 只筛选LOG_TAG为testApp的日志

`hilog -D 0xff1f` 只筛选 LOG_DOMAIN 为 0xff1f的日志

`hilog -p off` 关闭隐私模式,显示所有日志打印

`hilog -Q pidoff` 关闭日志流控限制

`hilog -b D -T testApp` 设置testApp标签日志的日志登记为Debug级别

## 适配指导

示例参考[cocos engine](https://github.com/cocos/cocos-engine) 日志系统鸿蒙化实现 [Log.cpp](https://github.com/cocos/cocos-engine/blob/develop/native/cocos/base/Log.cpp)

### 结构和接口设计

对外接口定义在`log_base.h`中,主要提供两种类型接口: 

1. 宏定义接口,包含6种日志等级的接口
```cpp
CC_LOG_DEBUG(formats, ...)
CC_LOG_INFO(formats, ...)   
CC_LOG_WARNING(formats, ...)
DO_CC_LOG_ERROR(formats, ...)
CC_LOG_FATAL(formats, ...)
```

2. `Log` 类,提供了日志等级控制等; `LogLevel`定义了日志级别.

### 实现

`log_base.h`中实现了日志逻辑,`static void logMessage(LogLevel level, const char *formats, ...)`接口的实现依赖平台特殊实现的`int LogPrintArgs(ohns::LogLevel level, const char *fmt, va_list ap)`

```cpp
void Log::logMessage(LogLevel level, const char *formats, ...) {

    va_list args;
    va_start(args, formats);
    LogPrintArgs(level, formats, args); // 依赖平台的特殊实现
    va_end(args);
}
```

比如OH迁移实现包含如下两方面,日志系统配置和`LogPrintArgs`实现

1. `log_ohos_config.h` 定义 `LOG_DOMAIN` 和 `LOG_TAG`

2. `log_ohos.cpp` 实现了`LogPrintArgs`方法

```cpp
int LogPrintArgs(ohns::LogLevel level, const char *fmt, va_list ap) {
    const static int32_t BUF_SIZE = 4096;
    char buf[BUF_SIZE] = {
        0,
    };
    vsnprintf(buf, BUF_SIZE, fmt, ap);

    LogLevel logLevel = LogLevel::LOG_DEBUG;
    switch (level) {
    case ohns::LogLevel::LEVEL_FATAL:
        logLevel = LogLevel::LOG_FATAL;
        break;
    case ohns::LogLevel::LEVEL_ERR:
        logLevel = LogLevel::LOG_ERROR;
        break;
    case ohns::LogLevel::LEVEL_WARN:
        logLevel = LogLevel::LOG_WARN;
        break;
    case ohns::LogLevel::LEVEL_INFO:
        logLevel = LogLevel::LOG_INFO;
        break;
    case ohns::LogLevel::LEVEL_DEBUG:
    default:
        logLevel = LogLevel::LOG_DEBUG;
        break;
    }

    int32_t ret = OH_LOG_Print(LogType::LOG_APP, logLevel, LOG_DOMAIN, LOG_TAG, "%{public}s", buf);
    return ret;
}
```

[示例工程](../../project/native-api/log)


## 链接
* [hilog native api reference](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/native-apis/_hi_log.md)
* [hilog js api reference](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/js-apis-hilog.md)
* [hilog 代码仓库](https://gitee.com/openharmony/hiviewdfx_hilog)
* [DevEco Studio日志工具](https://developer.harmonyos.com/cn/docs/documentation/doc-guides-V3/ide-debug-hilog-0000001172459337-V3?catalogVersion=V3)