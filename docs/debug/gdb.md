# GDB调试指南

本文主要介绍以下三点：

1、适配 OpenHarmony 版本的 GDB 工具的获取；

2、使用 IDE 工具生成支持 GDB 调试的应用 so；

3、使用 GDB 工具对 so 进行调试；

***

## <b>1.GDB工具获取</b>

当前OpenHarmony使用三方库musl来提供对标准C库的实现，但是针对标准的musl的release的版本做了部分改动，因此GDB网站上提供的工具在OpenHarmony上使用可能会导致部分符号无法识别。

针对上述问题，OpenHarmony开发者提供了对应版本的GDB工具：[GDB(2023.04.19)](https://gitee.com/link?target=https%3A%2F%2Fpan.baidu.com%2Fs%2F12mVZzbHak94Ky7mI3ge6dg%3Fpwd%3Ducit)

![GDB编译目录结构](../figures/debug/gdb/GDB_file_introduce.PNG ':size=60%')

> 上述版本如果无法满足对应诉求，可以参考[OpenHarmony GDB编译指导](https://gitee.com/stesen/ohos_cross_tools)，搭建 OpenHarmony GDB 编译环境，编译对应支持版本。

***

## <b>2.GDB工具推送</b>

下载对应版本GDB文件后，将对应文件推送到设备的bin目录下。

```shell
hdc shell mount -o rw,remount /

# 将目标版本gdb推送到data目录下
hdc file send debugroot_aarch64.tgz /data

# 将文件解压到data目录下
hdc shell tar zxvf /data/debugroot_aarch64.tgz -C ./

# 初始化GDB运行环境变量,如果路径不在data下，需要手动修改init中的路径
./data/debugroot/init.sh

# 删除so的安全管控
hdc shell rm -rf /system/lib/libsystem_filter.z.so

# 重启设备
hdc shell reboot
```

***

## <b>3.调试应用</b>

当前可以采用以下两种方式对应用进行断点调试：

- 启动应用后，通过gdb attach的方式断点到对应的应用:
  
  - 获取应用进程id
  ```shell
  # 获取应用运行pid
  ps -ef|grep "应用包名"
  ```
  ![获取应用进程号](../figures/debug/gdb/get_app_pid.png ':size=40%')
 
  - 对运行应用进行断点
  ```shell
  gdb attach pid
  ```
  ![应用断点](../figures/debug/gdb/gdb_attach_application.png ':size=40%')

  - 获取当前调用堆栈信息
  ```shell
  # 这个配置文件中改为：
  registry=https://repo.harmonyos.com/ohpm/strict_ssl=false
  ```
  ![获取应用运行栈](../figures/debug/gdb/heap_top_info.png ':size=50%')

- 通过父进程对子进程进行断点调试

  - 当前OH上所有的应用的进程的父进程均是appspawn进程，因此可以通过断点到该进程，调试对应子进程
    ```shell
    # 获取appspawn进程的进程号
    ps -ef|grep 'appspawn'
    ```
  ![获取appspawn进程号](../figures/debug/gdb/appspawn-pid.png ':size=40%')

    ```shell
    gdb attach pid
    # 设置gdb在fork后，调试子进程
    set follow-fork-mode child
    ```


> 注：两种方式均可以实现对应断点，有以下区别: 
> 1. 方法1可以对指定应用进行断点，方法二可以实现对所有应用进程的断点
> 2. 使用方法差异：方法1 需要先打开应用，再进行attach断点。方法2需要先attach到父进程，再打开应用进行断点。

***

## <b> 4. 常见使用问题 </b>

- 常见gdb使用命令: [GDB常用指令集合](https://zhuanlan.zhihu.com/p/297925056)

- gdb 日志导出

```shell
# 日志导出到指定目录
set logging file /data/gdb.log
set logging on
```






