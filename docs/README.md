# OpenHarmony NDK Samples

***

**NDK** (Native Development Kit) is a set of tools that enable developers to code using C/C++ in OpenHarmony applications and provides generous [platform libraries](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/native-apis/Readme-CN.md). For beginners in OpenHarmony programming, using JavaScript code and framework APIs is sufficient to develop applications. However, if the following goals are required, NDK will play an significant part:

- Improve application performance and run computationally intensive applications such as games;
- Reuse your own or other developers' C and C++libraries.

Developers may use NDK in **DevEco Studio** to compile C/C++ code into the so library, and then use DevEco Studio's **hvigor-ohos-plugin** to package the shared library into the OpenHarmony applications. Then, the ArkTS code can call functions in the shared library through the **Node-API** (formerly [N-API](https://nodejs.org/api/n-api.html)) framework. 

**OpenHarmony NDK samples** provide suggestions and the guidance for developers to migrate native applications to OHOS. The purpose of this guidance is to write concise tips, easy to find resources, and refreshing your mindset. So that you can migrate or build your native applications naturally.

***

Pages on this handbook are built based on [docsify](https://docsify.js.org/#/). For any issues you found in these samples, please create a Pull Request in our [Gitee repository](https://gitee.com/Wentao-Du/the-north-remembers). Also please check our [issues](https://gitee.com/Wentao-Du/The-North-Remember/issues), we have some interesting topics to discuss. 
