#include "napi/native_api.h"
#include "common.h"
#include <hilog/log.h>
static napi_value handleScope(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value object;
    napi_get_cb_info(env, info, &argc, &object, nullptr, nullptr);
    
    napi_handle_scope scope;
    napi_open_handle_scope(env, &scope);
    
    napi_value eleVal;
    napi_get_element(env, object, 2, &eleVal);
    
    napi_close_handle_scope(env, scope);
    
    int32_t res;
    napi_get_value_int32(env, eleVal, &res);
    return eleVal;
}

static napi_value handleEscapableScope(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value object;
    napi_get_cb_info(env, info, &argc, &object, nullptr, nullptr);
    
    napi_escapable_handle_scope scope;

    napi_value outerEleVal;
    
    napi_open_escapable_handle_scope(env, &scope);
    napi_value eleVal;
    napi_get_element(env, object, 2, &eleVal);
    napi_escape_handle(env, scope, eleVal, &outerEleVal);
    napi_close_escapable_handle_scope(env, scope);
    
    int32_t res;
    napi_get_value_int32(env, outerEleVal, &res);
    
    return outerEleVal;
}

napi_ref ref;

struct AsyncWorkInfo {
    napi_async_work work;
};

struct AsyncWorkInfo data = { nullptr };

static void AExecute(napi_env env, void *data) {
    struct AsyncWorkInfo *arg = (struct AsyncWorkInfo *)data;
    napi_value recv;
    
    napi_get_reference_value(env, ref, &recv);
    
    int32_t res;
    napi_get_value_int32(env, recv, &res);
//    OH_LOG_INFO(LOG_APP, "ref val is %{pubilc}d", res);
}
static void BComplete(napi_env env, napi_status status, void *data) {
    
}


static napi_value handleReference(napi_env env, napi_callback_info info)
{
    napi_value resourceName;
    napi_create_string_utf8(env, "asyncWork", NAPI_AUTO_LENGTH, &resourceName);
    
    napi_value test_data;
    napi_create_int32(env, 333, &test_data);
    napi_create_reference(env, test_data, 1, &ref);
    
    struct AsyncWorkInfo *ptr = &data;
    napi_create_async_work(env, nullptr, resourceName, AExecute, BComplete, ptr, &ptr->work);
    napi_queue_async_work(env, ptr->work);
    return resourceName;
}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
        { "handleScope", nullptr, handleScope, nullptr, nullptr, nullptr, napi_default, nullptr },
        { "handleEscapableScope", nullptr, handleEscapableScope, nullptr, nullptr, nullptr, napi_default, nullptr },
        { "handleReference", nullptr, handleReference, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version =1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = { 0 },
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void)
{
    napi_module_register(&demoModule);
}
