export const handleScope: (a:Object) => number;
export const handleEscapableScope: (a:Object) => void;

export const handleReference: () => void;