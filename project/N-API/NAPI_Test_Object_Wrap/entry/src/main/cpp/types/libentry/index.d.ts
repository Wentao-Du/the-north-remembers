export const getNewInstance : (a: number, b: number) => MyObject;
export class MyObject {
  num1: number;
  num2: number;

  constructor(a: number, b: number);

  operator(operate: number): number;
}
