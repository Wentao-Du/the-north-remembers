#include "napi/native_api.h"

#ifndef NAPIObjectBindDemo_object_wrap_H
#define NAPIObjectBindDemo_object_wrap_H

class MyObject {
public:
  // 初始化函数
  static void Init(napi_env env, napi_value exports);
  // 构造函数引用
  static napi_ref constructor_ref;
private:
  explicit MyObject(double a = 0, double b = 0);
  ~MyObject();
  // 释放资源的函数(类似类的析构函数)
  static void Destructor(napi_env env, void *finalize_data, void *finalize_hint);
  // new一个新的JS对象时实际的构造函数
  static napi_value JSConstructor(napi_env env, napi_callback_info info);
  // JS调用类的方法
  static napi_value JSOperate(napi_env env, napi_callback_info info);
  
  double a;
  double b;
  napi_env env_;
  napi_ref wrapped_obj;
};

#endif // NAPIObjectBindDemo_object_wrap_H
