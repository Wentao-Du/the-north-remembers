export const add: (a: number, b: number) => number;

export const newAdd: (a: number, b: number) => number;

export const registerCallback: (a: (a?: number, b?: number) => number) => void;

export const handleCallbackWithoutParams: () => number;

export const handleCallbackWithParams: () => number;
