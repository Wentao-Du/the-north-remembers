#include "napi/native_api.h"
#include <thread>
#include <unistd.h>
#include <uv.h>
#include "./common.h"

struct CallbackContext {
    // JS N-API 上下文环境 
    napi_env env = nullptr;
    // JS 回调函数引用，保证 JS 主线程结束时，回调函数不被销毁
    napi_ref callbackRef = nullptr;
    // 下载进度数据
    int retData = 0;
};

// 模拟下载任务
void downloadTask(CallbackContext* context) {
    uv_loop_s* loop = nullptr;
    // 获取 context->env 上下文环境的 loop (线程池）
    napi_get_uv_event_loop(context->env, &loop);
    
    // uv_work_t 是关联 loop 和 线程池回调函数的结构体
    uv_work_t* work = new uv_work_t;
    work->data = (CallbackContext*)context;
    
    while (context && context->retData < 100) {
        uv_queue_work(
            loop,
            work, 
            [](uv_work_t* work) {}, 
            [](uv_work_t* work, int status){
                CallbackContext* context = (CallbackContext*)work->data;
                context->retData += 1;
                napi_handle_scope scope = nullptr;
                // 管理 napi_value 的生命周期，防止内存泄露
                napi_open_handle_scope(context->env, &scope);
                napi_value callback = nullptr;
                napi_get_reference_value(context->env, context->callbackRef, &callback);
                napi_value retArg;
                napi_create_int32(context->env, context->retData, &retArg);
                napi_value ret;
                // napi_call_function 调用 JS 侧回调，向其传输下载进度
                napi_call_function(context->env, nullptr, callback, 1, &retArg, &ret);
            
                napi_close_handle_scope(context->env, scope);
                if (context->retData > 99) {
                    delete context;
                    delete work;
                }
            }
        );
        // 控制 while 循环每 100 毫秒执行一次。
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
};

static napi_value startDownload(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1] = { 0 };
    napi_get_cb_info(env, info, &argc, args , nullptr, nullptr);
    auto asyncContext = new CallbackContext();
    asyncContext->env = env;
    napi_create_reference(env, args[0], 1, &asyncContext->callbackRef);
    // 启动下载线程
    std::thread downloadThread(downloadTask, asyncContext);
    downloadThread.detach();
    return nullptr;
};

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
        { "startDownload", nullptr, startDownload, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version =1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = { 0 },
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void)
{
    napi_module_register(&demoModule);
}
