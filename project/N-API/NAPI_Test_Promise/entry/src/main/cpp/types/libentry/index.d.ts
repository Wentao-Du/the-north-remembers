export const getPromise: () => Promise<any>;
export const handlePromise: (callback: Function) => void;