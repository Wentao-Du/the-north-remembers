#include "napi/native_api.h"
#include "./common.h"

static napi_value Add(napi_env env, napi_callback_info info)
{
    size_t requireArgc = 2;
    size_t argc = 2;
    napi_value args[2] = {nullptr};

    napi_get_cb_info(env, info, &argc, args , nullptr, nullptr);

    napi_valuetype valuetype0;
    napi_typeof(env, args[0], &valuetype0);

    napi_valuetype valuetype1;
    napi_typeof(env, args[1], &valuetype1);

    double value0;
    napi_get_value_double(env, args[0], &value0);

    double value1;
    napi_get_value_double(env, args[1], &value1);

    napi_value sum;
    napi_create_double(env, value0 + value1, &sum);

    return sum;

}

static napi_value throwExistingError(napi_env env, napi_callback_info info)
{
    napi_value message;
    napi_value error;
    NODE_API_CALL(env, napi_create_string_utf8(env, "existing error", NAPI_AUTO_LENGTH, &message));
    NODE_API_CALL(env, napi_create_error(env, NULL,  message, &error));
    NODE_API_CALL(env, napi_throw(env, error));
    return error;
}

static napi_value handleFnError(napi_env env, napi_callback_info info)
{
    size_t argc = 1;
    napi_value args[1];
    NODE_API_CALL(env, napi_get_cb_info(env, info, &argc, args, NULL, NULL));
    napi_value global;
    NODE_API_CALL(env, napi_get_global(env, &global));
    napi_value result;
    napi_status status = napi_call_function(env, global, args[0], 0, 0, &result);
    if (status == napi_pending_exception) {
        bool res;
        napi_value error;
        napi_value errMessage;
        char err_char_mes[10];
        size_t num;
        napi_is_exception_pending(env, &res);
        if (napi_get_and_clear_last_exception(env, &error) == napi_ok) {
            napi_get_named_property(env, error, "message", &errMessage);
            NODE_API_CALL(env, napi_get_value_string_utf8(env, errMessage, err_char_mes, 10, &num));
            return errMessage;
        }
    }
    return NULL;
}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
        { "add", nullptr, Add, nullptr, nullptr, nullptr, napi_default, nullptr },
        { "throwExistingError", nullptr, throwExistingError, nullptr, nullptr, nullptr, napi_default, nullptr },
        { "handleFnError", nullptr, handleFnError, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version =1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = { 0 },
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void)
{
    napi_module_register(&demoModule);
}
