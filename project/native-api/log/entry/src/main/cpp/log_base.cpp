#include "log_base.h"

#include <cstdio>
#include <cstdarg>
#include <stdarg.h>
#include <string>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif
int LogPrintArgs(ohns::LogLevel level, const char *fmt, va_list ap);

#ifdef __cplusplus
}
#endif

namespace ohns {

LogLevel Log::slogLevel = LogLevel::LEVEL_DEBUG;

void Log::logMessage(LogLevel level, const char *formats, ...) {

    va_list args;
    va_start(args, formats);
    LogPrintArgs(level, formats, args);
    va_end(args);
}

} // namespace ohns
