#ifndef cpp_log_config_h
#define cpp_log_config_h

#ifndef LOG_DOMAIN
#define LOG_DOMAIN 0xff1f // [0x0~0xFFFF] 领域划分
#endif

#ifndef LOG_TAG
#define LOG_TAG "testApp" // 应用标签 
#endif

#include "hilog/log.h"
#endif