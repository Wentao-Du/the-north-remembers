#include "log_ohos_config.h"

#include "log_base.h"

#include <cstdio>
#include <cstdint>

#ifdef __cplusplus
extern "C" {
#endif
int LogPrintArgs(ohns::LogLevel level, const char *fmt, va_list ap);

#ifdef __cplusplus
}
#endif

int LogPrintArgs(ohns::LogLevel level, const char *fmt, va_list ap) {
    const static int32_t BUF_SIZE = 4096;
    char buf[BUF_SIZE] = {
        0,
    };
    vsnprintf(buf, BUF_SIZE, fmt, ap);

    LogLevel logLevel = LogLevel::LOG_DEBUG;
    switch (level) {
    case ohns::LogLevel::LEVEL_FATAL:
        logLevel = LogLevel::LOG_FATAL;
        break;
    case ohns::LogLevel::LEVEL_ERR:
        logLevel = LogLevel::LOG_ERROR;
        break;
    case ohns::LogLevel::LEVEL_WARN:
        logLevel = LogLevel::LOG_WARN;
        break;
    case ohns::LogLevel::LEVEL_INFO:
        logLevel = LogLevel::LOG_INFO;
        break;
    case ohns::LogLevel::LEVEL_DEBUG:
    default:
        logLevel = LogLevel::LOG_DEBUG;
        break;
    }

    int32_t ret = OH_LOG_Print(LogType::LOG_APP, logLevel, LOG_DOMAIN, LOG_TAG, "%{public}s", buf);
    return ret;
}