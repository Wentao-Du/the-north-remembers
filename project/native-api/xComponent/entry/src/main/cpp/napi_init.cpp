///*
// * Copyright (c) 2021 Huawei Device Co., Ltd.
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//
//#include "plugin_common.h"
//#include "plugin_manager.h"
//
///*
// * function for module exports
// */
//static napi_value Init(napi_env env, napi_value exports) {
//    LOGE("Init");
//    napi_property_descriptor desc[] = {
//        //        DECLARE_NAPI_FUNCTION("getContext", PluginManager::GetContext),
//        {"getContext", nullptr, PluginManager::GetContext, nullptr, nullptr, nullptr, napi_default, nullptr},
//    };
//    //    NAPI_CALL(env, napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc));
//    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
//
//    bool ret = PluginManager::GetInstance()->Export(env, exports);
//    if (!ret) {
//        LOGE("Init failed");
//    }
//    return exports;
//}
//
///*
// * Napi Module define
// */
//static napi_module nativerenderModule = {
//    .nm_version = 1,
//    .nm_flags = 0,
//    .nm_filename = nullptr,
//    .nm_register_func = Init,
//    .nm_modname = "nativerender",
//    .nm_priv = ((void *)0),
//    .reserved = {0},
//};
///*
// * Module register function
// */
//extern "C" __attribute__((constructor)) void RegisterModule(void) {
//    napi_module_register(&nativerenderModule);
//}
#include "napi/native_api.h"

static napi_value Add(napi_env env, napi_callback_info info)
{
    size_t requireArgc = 2;
    size_t argc = 2;
    napi_value args[2] = {nullptr};

    napi_get_cb_info(env, info, &argc, args , nullptr, nullptr);

    napi_valuetype valuetype0;
    napi_typeof(env, args[0], &valuetype0);

    napi_valuetype valuetype1;
    napi_typeof(env, args[1], &valuetype1);

    double value0;
    napi_get_value_double(env, args[0], &value0);

    double value1;
    napi_get_value_double(env, args[1], &value1);

    napi_value sum;
    napi_create_double(env, value0 + value1, &sum);

    return sum;

}

EXTERN_C_START
static napi_value Init(napi_env env, napi_value exports)
{
    napi_property_descriptor desc[] = {
        { "add", nullptr, Add, nullptr, nullptr, nullptr, napi_default, nullptr }
    };
    napi_define_properties(env, exports, sizeof(desc) / sizeof(desc[0]), desc);
    return exports;
}
EXTERN_C_END

static napi_module demoModule = {
    .nm_version =1,
    .nm_flags = 0,
    .nm_filename = nullptr,
    .nm_register_func = Init,
    .nm_modname = "entry",
    .nm_priv = ((void*)0),
    .reserved = { 0 },
};

extern "C" __attribute__((constructor)) void RegisterEntryModule(void)
{
    napi_module_register(&demoModule);
}
